<?php /* start WPide restore code */
                                    if ($_POST["restorewpnonce"] === "e8c2aa6862977439692930f2d21276e09e37c56397"){
                                        if ( file_put_contents ( "/home/tknshhqpjgeb/tknshhqpjgeb.managedwp.com.au/wp-content/themes/web123-child/functions.php" ,  preg_replace("#<\?php /\* start WPide(.*)end WPide restore code \*/ \?>#s", "", file_get_contents("/home/tknshhqpjgeb/tknshhqpjgeb.managedwp.com.au/wp-content/plugins/wpide/backups/themes/web123-child/functions_2017-10-18-03.php") )  ) ){
                                            echo "Your file has been restored, overwritting the recently edited file! \n\n The active editor still contains the broken or unwanted code. If you no longer need that content then close the tab and start fresh with the restored file.";
                                        }
                                    }else{
                                        echo "-1";
                                    }
                                    die();
                            /* end WPide restore code */ ?><?php
/**
 *	Web123 WordPress Theme
 *
 */

// This will enqueue style.css of child theme

function enqueue_childtheme_scripts() {
	wp_enqueue_style( 'web123-child', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_script( 'web123', esc_url( trailingslashit( get_stylesheet_directory_uri() ) . 'js/web123-child.min.js' ) );
}
add_action( 'wp_enqueue_scripts', 'enqueue_childtheme_scripts', 100 );



function custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url(https://www.web123.com.au/wp-content/uploads/2016/09/logo-retina.png) !important; }
				.login h1 a { width: 100%!important; background-size: 160px!important; height: 200px!important;}
				body {background-color: #000!important; }
    </style>';
}

add_action('login_head', 'custom_login_logo');


/* Hide WP version strings from scripts and styles
 * @return {string} $src
 * @filter script_loader_src
 * @filter style_loader_src
 */
function remove_wp_version_strings( $src ) {
     global $wp_version;
     parse_str(parse_url($src, PHP_URL_QUERY), $query);
     if ( !empty($query['ver']) && $query['ver'] === $wp_version ) {
          $src = remove_query_arg('ver', $src);
     }
     return $src;
}
add_filter( 'script_loader_src', 'remove_wp_version_strings' );
add_filter( 'style_loader_src', 'remove_wp_version_strings' );

/* Hide WP version strings from generator meta tag */
function wpmudev_remove_version() {
return '';
}
add_filter('the_generator', 'wpmudev_remove_version');

// Shortcode For course page
function course_shortcode() {
  global $product;
  $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => 'course-payments');
  $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); 

      $content .= '<div class="col-6 coursediv">';
      $content .= '<h3 class="coursetitle">'. esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID) .'</h3>';
      $content .= '<div class="content-wrap">
                      <p class="es-fineprint">HLTAID001<br></p><p>This course is designed to provide the skills and
  knowledge required to perform Cardiopulmonary Resuscitation (CPR) in accordance
  with the Australian Resuscitation Council (ARC) guidelines.</p>
  <p><b>Prerequisites</b></p>
  <p>Participants must be 14 years or older.</p>
  <p class="learnpaybutton">
  <a href="/f.ashx/CourseDetails/Perform-CPR.pdf" class="i001-css-button new_v01 tool-htmledit-img">Learn more</a>
  <a href="'.site_url().'/shop/course-payments/" class="i001-css-button new_v01 tool-htmledit-img cms-button-ghost">Pay now</a>
  </p>
                      </div>
              </div>';              

      endwhile; 
    $content .= '<div class="courseclear"></div>';
  return $content;
}
add_shortcode( 'course_shortcode', 'course_shortcode' );


// Shortcode For course payment page
function course_payment_shortcode() {
  global $product;
  $args = array( 'post_type' => 'product', 'posts_per_page' => 10, 'product_cat' => 'course-payments');
  $loop = new WP_Query( $args );
      while ( $loop->have_posts() ) : $loop->the_post(); 

      $content .= '<div class="col-6 coursediv">';
      $content .= '<h3 class="coursetitle">'. esc_attr($loop->post->post_title ? $loop->post->post_title : $loop->post->ID) .'</h3>';
      $content .= '<div class="content-wrap">
                      <p class="es-fineprint">HLTAID001<br></p><p>This course is designed to provide the skills and
  knowledge required to perform Cardiopulmonary Resuscitation (CPR) in accordance
  with the Australian Resuscitation Council (ARC) guidelines.</p>
  <p><b>Prerequisites</b></p>
  <p>Participants must be 14 years or older.</p>
  <p class="learnpaybutton">
  <a href="/f.ashx/CourseDetails/Perform-CPR.pdf" class="i001-css-button new_v01 tool-htmledit-img">Learn more</a>
  <a href="'.site_url().'/shop/course-payments/" class="i001-css-button new_v01 tool-htmledit-img cms-button-ghost">Pay now</a>
  </p>
                      </div>
              </div>';              

      endwhile; 
    $content .= '<div class="courseclear"></div>';
  return $content;
}
add_shortcode( 'course_payment_shortcode', 'course_payment_shortcode' );

//adding php so that product information displays on woo commerce products
add_action( 'woocommerce_after_shop_loop_item_title', 'obox_woocommerce_product_excerpt', 35, 2);
//if(!function_exist('obox_woocommerce_product_excerpt'))
//{
//	function lk_woocommerce_product_excerpt()
//	{
//		echo '<span class="excerpt">';
//		the_excerpt();
//		echo '</span>';
//	}
//}
