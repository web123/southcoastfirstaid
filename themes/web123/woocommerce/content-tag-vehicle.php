<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<?php
 do_action( 'woocommerce_archive_description' ); ?>

            <?php if ( have_posts() ) : ?>

                <?php do_action('woocommerce_before_shop_loop'); ?>
                <div class="restock-form-wrap">

                <ul class="restock-product-list">
                	<li class="header row">
						<ul>
							<li class="item"><div>Item</div></li>
							<li class="name"><div>Item Name</div></li>
							<li class="price"><div>Price</div></li>
							<li class="add"><div>Add</div></li>
						</ul>
					</li>

                    <?php woocommerce_product_subcategories(); ?>

                    <?php while ( have_posts() ) : the_post(); ?>
                    	<?php global $post; 
                    			global $product;


                    		 $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
                    		 ?>

    
                    	<li class="row">
			    <ul>
				<li class="item">
					<div class="img-wrap">
						<img src="<?php  echo $image[0]; ?>" data-id="<?php echo  $post->ID; ?>">  

						
					</div>
				</li>
				<li class="name"><div><a href="<?php echo  get_permalink( $post->ID );  ?>" target="_blank"><?php echo  $post->post_title; ?></a></div></li>
				<li class="price"><div><?php echo get_woocommerce_currency_symbol(). $product->get_price(); ?></div></li>
				<li class="add">
						<div><?php echo apply_filters( 'woocommerce_loop_add_to_cart_link',
							sprintf( '<span class="add-to-cart-button-outer"><span class="add-to-cart-button-inner"><a rel="nofollow" href="%s" data-quantity="%s" data-product_id="%s" data-product_sku="%s" class="qbutton add-to-cart-button %s">%s</a></span></span>',
								esc_url( $product->add_to_cart_url() ),
								esc_attr( isset( $quantity ) ? $quantity : 1 ),
								esc_attr( $product->id ),
								esc_attr( $product->get_sku() ),
								esc_attr( (isset( $class ) ? str_replace('ajax_add_to_cart', '',$class) : 'button').$button_classes ),
								esc_html( $product->add_to_cart_text() )
								),
								$product ); ?>
						</div>
				</li>
			
			    </ul>
		</li>
                        <?php //wc_get_template_part( 'content', 'product' ); ?>

                    <?php endwhile; // end of the loop. ?>

                <?php// woocommerce_product_loop_end(); ?>
            </ul>
        </div>

                <?php do_action('woocommerce_after_shop_loop'); ?>

            <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

                <?php wc_get_template( 'loop/no-products-found.php' ); ?>

            <?php endif;
